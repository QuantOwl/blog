---
title: "Driver Test"
author: "Johan Mare"
date: "Thursday, November 17, 2016"
output: html_document
---

# Test the driver program.

The Table of Contents above is generated in the *upload_post.R* code. You decide if you want it.

This post will be uploaded as my first post and will change as I'm testing the driver program.

## Text
To create documents here a basic knowledge of the following are essential:
- [Markdown](http://daringfireball.net/projects/markdown/),
- [knitr](http://yihui.name/knitr/).

## Images
For images I use external R code in knitr and R markdown. A description can be found on the 
[knitr](http://yihui.name/knitr/) site. Another brilliant description can be found at [ZevRoss](http://zevross.com/blog/2014/07/09/making-use-of-external-r-code-in-knitr-and-r-markdown/). While you are there
check out some of his other posts as well.

Use the following steps in your post to import the images:

### Load the external code 
The echo=FALSE will cause that nothing will be seen in the output, but the external code will be read.

```{r echo=FALSE}
read_chunk('tmp_knitr.R')
```

### Load the first image

```{r img1, results='asis', echo=FALSE}

```
Proudly produced by myself.

### Load the next image

```{r img2, results='asis', echo=FALSE}

```

You can load as many images as you like, as long as they are declared in *upload_post.R*. Standardizing on **img** with a number works for me. 

Good luck

